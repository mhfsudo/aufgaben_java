package ch.mhf.exception;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/*
    Throwable
        Exception
            RuntimeException (freiwillig)
        Error

            Audiosystem.getClip()           ==> !LineUnavailableException (SecurityException, IllegalArgumentException)
            clip.open()                     ==> !LineUnavailableException, !IOException
            Audiosystem.getAudioInputStream ==> !UnsupportedAFEException, !IOException, (NullPointerException)
            File                            ==> (NullPointerException)
            clip.start()                    ==> -
            TimeUnit.sleep()                ==> !InterruptedException
            clip.getMicrosecondLength()     ==> -
            clip.close()                    ==> (SecurityException)

    Checked Exception = geprüfte Exception = Nicht-Runtime-Exception ==> verpflichtend(!) abfangen/behandeln oder weitergeben
    Unchecked Exception = ungeprüfte Exception = Runtime-Exception ==> freiwillig behandeln (oder weitergeben)
 */

public class ExceptionExample
{

    public static void main(String[] args) //throws LineUnavailableException, IOException, UnsupportedAudioFileException, InterruptedException
    //public static void main(String[] args) //throws Exception, kein schöner Still
    {
        try {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(new File("alf-lach.wav")));   //UnsupportedAudioFileException,IOException
            clip.start();
            TimeUnit.MICROSECONDS.sleep(clip.getMicrosecondLength() + 50);
            clip.close();
        }
        catch (LineUnavailableException e)
        {
            System.err.println("Fehler");
        }
        catch (IOException e)
        {
            System.err.println("Datei nicht gefunden");
        }
        catch (UnsupportedAudioFileException e)
        {
            System.err.println("Falsches Audio Format");
        }
        catch (InterruptedException e)
        {
            System.err.println("Exception");
        }
    }
}